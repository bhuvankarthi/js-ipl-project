

const path = require("path")
const fs = require("fs")


const matchesInYear = (match) => {

    let final = {};

    match.map(item => {

        let key = "Matches Played in the " + item.season

        if (final.hasOwnProperty(key)) {

            final[key] += 1

        } else {

            final[key] = 1

        }

    })
    const pathfile = path.join(__dirname, "../public/output/question1.json")

    fs.writeFile(pathfile, JSON.stringify(final), { encoding: "utf-8" }, (error) => {

        if (error) {

            console.log(error);

        }
    })

}





const winsPerTeam = (matches) => {

    let result = {};
    matches.map(el => {

        if (result[el.season]) {

            if (result[el.season][el.winner]) {

                result[el.season][el.winner]++

            } else {

                result[el.season][el.winner] = 1

            }

        } else {

            result[el.season] = {}


            result[el.season][el.winner] = 1



        }

    })



    fs.writeFile("../public/output/question2.json", JSON.stringify(result), { encoding: "utf-8" }, (error) => {

        if (error) {

            console.log(error);

        }
    })
}





const extraRunsPerTeam = (matches, deliveries) => {

    const ids = [];

    matches.filter(el => {

        if (Number(el.season) === 2016) {

            ids.push(el.id)

        }

    })

    const extras = {}



    ids.filter(item => {

        const rows = deliveries.filter(e => e.match_id === item)

        rows.map(el => {

            (extras[el.bowling_team]) ? extras[el.bowling_team] = extras[el.bowling_team] + Number(el.extra_runs) : extras[el.bowling_team] = Number(el.extra_runs);


        })


    })
    // return extras
    const pathfile = path.join(__dirname, "../public/output/question3.json")



    fs.writeFile(pathfile, JSON.stringify(extras), "utf8", function (error) {
        if (error) console.log(error);

    })
}



//economicalBowlers

const economicalBowlers = (matches, deliveries) => {

    const matchIds = []

    const match = []

    bowlers = {};

    matches.map(el => {

        if (Number(el.season) === 2015) {


            matchIds.push(el.id)

        }

    })

    matchIds.map(item => {


        (deliveries.filter(ele => {

            if (ele.match_id === item) {

                match.push(ele)


            }

        }))


    })


    match.map(el => {

        if (bowlers[el.bowler]) {

            const bowled = el.ball <= 6 ? bowlers[el.bowler].bowled + 1 : bowlers[el.bowler].bowled;

            const runs = bowlers[el.bowler].runs + Number(el.total_runs)



            bowlers[el.bowler] = { bowled, runs }


        } else {

            const bowled = 1;


            const runs = Number(el.total_runs)

            bowlers[el.bowler] = { bowled, runs }
        }
    })
    let arr = []

    let final = {};

    Object.keys(bowlers).map(keys => {

        const oversBowled = (bowlers[keys].bowled / 6).toFixed(1)

        const bowler = bowlers[keys]

        const economy = (Number(bowler.runs) / oversBowled).toFixed(1)

        bowlers[keys] = { ...bowler, overs: oversBowled, economy: economy }

        arr.push({ bowler: keys, ...bowlers[keys] })
    })
    const sorted = arr.sort((a, b) => {

        if (Number(a.economy) < Number(b.economy)) return -1;

        if (Number(a.economy) > Number(b.economy)) return 1;

        return 0;
    })
    let topEconomy = (arr.splice(0, 10));

    const pathfile = path.join(__dirname, "../public/output/question4.json")

    fs.writeFile(pathfile, JSON.stringify(topEconomy), { encoding: "utf-8" }, (error) => {
        if (error) {
            console.log(error);
        }
    })


}






const tossAndMatch = (matches) => {

    let result = {};


    matches.map(el => {

        if (el.toss_winner === el.winner) {

            if (result[el.winner]) {

                result[el.winner]++

            } else {

                result[el.winner] = 1;

            }
        }
    })
    // return (result);

    const pathfile = path.join(__dirname, "../public/output/question5.json")

    fs.writeFile(pathfile, JSON.stringify(result), { encoding: "utf-8" }, (error) => {

        if (error) {

            console.log(error);

        }
    })

}






const manOfTheMatch = (matches) => {

    let players = {};

    matches.map(match => {

        if (players[match.season]) {

            if (players[match.season][match.player_of_match]) {

                players[match.season][match.player_of_match]++

            } else {

                players[match.season][match.player_of_match] = 1

            }

        } else {

            players[match.season] = {}


            players[match.season][match.player_of_match] = 1





        }

    })


    const res = (Object.entries(players)).map(el => Object.entries(el[1]).sort((a, b) => b[1] - a[1]))





    let result = {};

    const keys = Object.keys(players);



    res.map((el, index) => {
        result[keys[index]] = el[0][0]
    })







    const pathfile = path.join(__dirname, "../public/output/question6.json")

    fs.writeFile(pathfile, JSON.stringify(result), { encoding: "utf-8" }, (error) => {

        if (error) {

            console.log(error);

        }
    })
}






const strikeRate = (matches, deliveries) => {
    let final = {}

    const batsman = "DA Warner"

    let array = deliveries.filter(item => {
        return item.batsman === "DA Warner"
    })


    const ids = {}



    matches.map(el => {
        ids[el.id] = el.season
    })


    array.reduce((acc, curr) => {
        if (acc[ids[curr.match_id]]) {
            const balls = acc[ids[curr.match_id]].balls + 1;
            const runs = acc[ids[curr.match_id]].runs + Number(curr.batsman_runs);
            const strikerate = ((runs / (balls)) * 100).toFixed(1);
            acc[ids[curr.match_id]] = { balls, runs, strikerate }


        } else {
            const balls = 1;
            const runs = Number(curr.batsman_runs);
            const strikerate = ((runs / (balls)) * 100).toFixed(1);
            acc[ids[curr.match_id]] = { balls, runs, strikerate }

        }

        return final[batsman] = acc


    }, {})


    const pathfile = path.join(__dirname, "../public/output/question7.json")
    fs.writeFile(pathfile, JSON.stringify(final), { encoding: "utf-8" }, (error) => {
        if (error) {
            console.log(error);
        }
    })
}


const playerDismissed = (deliveries) => {
    let final = {};

    let dismissalRows = []


    deliveries.map(el => {

        if (el.player_dismissed && el.dismissal_kind !== "runout") {

            dismissalRows.push(el)
        }
    })

    // console.log(out);


    dismissalRows.map(wicket => {

        if (final[wicket.batsman]) {
            if (final[wicket.batsman][wicket.bowler]) {
                final[wicket.batsman][wicket.bowler]++
            } else {
                final[wicket.batsman][wicket.bowler] = 1
            }


        } else {
            final[wicket.batsman] = {}
            final[wicket.batsman][wicket.bowler] = 1
        }
    })
    



    let max = -1
    let batsman = "";
    let bowler = ""
    let dismissal = []

    


    for (let keys in final) {


        dismissal.push((Object.entries(final[keys])).sort((a, b) => b[1] - a[1])[0])


        

        dismissal.map((ball) => {

           
                if (ball[1] > max) {
                    batsman = keys;
                    bowler = ball[0];
                    max = ball[1]
                }
            
        })

    }





    let temp = (`${batsman} has been dismissed by ${bowler} ${max} times .`);





 console.log(temp);

    const pathfile = path.join(__dirname, "../public/output/question8.json")
    fs.writeFile(pathfile, JSON.stringify(temp), { encoding: "utf-8" }, (error) => {
        if (error) {
            console.log(error);
        }

    })



}







const superOverEconomicalBowler = (deliveries) => {
    let bowlers = {};

    const rows = [];
    deliveries.filter(el => {
        if (el.is_super_over > 0) {
            rows.push(el)
        }
    })


    rows.map(el => {
        if (bowlers[el.bowler]) {


            const bowled = el.ball <= 6 ? bowlers[el.bowler].balls + 1 : bowlers[el.bowler].balls

            const runs = bowlers[el.bowler].runs + Number(el.total_runs)
            const overCalc = (Math.floor(bowled / 6))
            const economy = (runs / (overCalc + (((bowled - (overCalc * 6)) / 10)))).toFixed(1)

            bowlers[el.bowler] = { "runs": runs, "balls": bowled, "economy": economy }

        } else {
            const runs = Number(el.total_runs)
            const bowled = 1;

            bowlers[el.bowler] = { "runs": runs, "balls": bowled }
        }
    })

    // console.log(bowlers);


    let entries = (Object.entries(Object.values(bowlers)))


    let min = Number.POSITIVE_INFINITY
    const index = entries.map(el => {

        if (min > Number(el[1].economy)) {
            min = el[1].economy
        }
    })


    const result = {}



    Object.keys(bowlers).map(el => {

        if ((bowlers[el].economy) === min) {

            result[el] = min;

        }
    })

    // console.log(result);

    const pathfile = path.join(__dirname, "../public/output/question9.json")
    fs.writeFile(pathfile, JSON.stringify(result), { encoding: "utf-8" }, (error) => {
        if (error) {
            console.log(error);
        }
    })

}








module.exports = {
    matchesInYear: matchesInYear,
    winsPerTeam: winsPerTeam,
    extraRunsPerTeam: extraRunsPerTeam,
    economicalBowlers: economicalBowlers,
    tossAndMatch: tossAndMatch,
    manOfTheMatch: manOfTheMatch,
    strikeRate: strikeRate,
    superOverEconomicalBowler: superOverEconomicalBowler,
    playerDismissed: playerDismissed
}
